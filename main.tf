provider "vsphere" {
  user           = var.vsphere_user
  password       = var.vsphere_password
  vsphere_server = var.vsphere_server

  # If you have a self-signed cert
  allow_unverified_ssl = true
}

data "vsphere_datacenter" "dc" {
  name           = var.datacenter
}

data "vsphere_resource_pool" "pool" {
  name          = var.pool
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_datastore" "datastore_primary" {
  for_each      = var.machines
  name          = each.value.datastore_1
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_datastore" "datastore_secondary" {
  for_each      = var.machines
  name          = each.value.datastore_2
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "network" {
  for_each      = var.machines
  name          = each.value.network
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_virtual_machine" "template" {
  for_each      = var.machines
  name          = each.value.template
  datacenter_id = data.vsphere_datacenter.dc.id
}

resource "vsphere_virtual_machine" "machines" {
  for_each = var.machines
  name             = "${each.value.hostname}.${each.value.domain}"
  resource_pool_id = data.vsphere_resource_pool.pool.id
  datastore_id     = data.vsphere_datastore.datastore_primary[each.key].id

  num_cpus = each.value.num_cpus
  memory   = each.value.memory
  guest_id = data.vsphere_virtual_machine.template[each.key].guest_id
  scsi_type = data.vsphere_virtual_machine.template[each.key].scsi_type
  firmware = data.vsphere_virtual_machine.template[each.key].firmware
  folder = each.value.folder

  network_interface {
    network_id   = data.vsphere_network.network[each.key].id
    adapter_type = data.vsphere_virtual_machine.template[each.key].network_interface_types[0]
  }

  disk {
    label            = "disk0"
    size             = data.vsphere_virtual_machine.template[each.key].disks[0].size
    eagerly_scrub    = data.vsphere_virtual_machine.template[each.key].disks[0].eagerly_scrub
    thin_provisioned = data.vsphere_virtual_machine.template[each.key].disks[0].thin_provisioned
    unit_number      = 0
  }

  dynamic "disk" {
    for_each = each.value.disks
    content {
      label            = disk.value["label"]
      size             = disk.value["size"]
      unit_number      = disk.value["unit_number"]
      datastore_id     = data.vsphere_datastore.datastore_secondary[each.key].id
    }
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template[each.key].id
    customize {
      linux_options {
        host_name = each.value.hostname
        domain    = each.value.domain
      }

      network_interface {
        ipv4_address = each.value.ipv4_address
        ipv4_netmask = each.value.ipv4_netmask
        dns_domain   = each.value.dns_domain
      }

      ipv4_gateway = each.value.ipv4_gateway
      dns_server_list = each.value.dns_servers
    }
  }

  provisioner "remote-exec" {
    inline = [
      "systemd-machine-id-setup",
      "sudo mkdir /root/.ssh",
      "sudo touch /root/.ssh/authorized_keys",
      "echo ${var.ssh_pub_key} | sudo tee /root/.ssh/authorized_keys",
      "sudo chown root:root -R /root/.ssh",
      "sudo chmod 700 /root/.ssh",
      "sudo chmod 600 /root/.ssh/authorized_keys",
      "sudo passwd --lock root"
    ]

    connection {
      host        = self.default_ip_address
      type        = "ssh"
      user        = var.admin_user
      password    = var.admin_password
    }
  }
}
